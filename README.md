SS： https://github.com/malaohu/ss-with-net-speeder

SSR： https://github.com/malaohu/ssr-with-net-speeder


# 安装部署

## 源码部署
```
git clone https://github.com/malaohu/Arukas-API
cd Arukas-API
npm install
node server.js token secret 1
```
然后访问：ip:6767 即可



## Docker部署
```
docker run -d --name arukas-api -p 6767:6767 malaohu/arukas-api --env TOKEN=token SECRET=secret IS_CRON=1
```


```
镜像：malaohu/arukas-api
端口:6767 TCP
环境变量:TOKEN=token SECRET=secret IS_CRON=1

#也可以使用CMD启动(不推荐)
CMD : node /app/server.js token secret 1
```

## 环境变量(ENV)
TOKEN 和  SECRET 获取地址：https://app.arukas.io/settings/api-keys


IS_CRON 传 0 或者 1
是否启动自动启动服务功能，0是不启动，1是启动。
该服务会定时检测APP运行状态，如果APP没有启动，会发送一个启动命令。


## 功能介绍
* 实时获取IP和端口，以及SS(R)的配置信息
* 检查APP运行情况，没有启动的发送启动命令(支持手动请求)

## 请求地址
* http://ip:6767
* http://ip:6767/check/status 手动检查服务运行状态，没有启动服务，发送启动命令
* http://ip:6767/ssr/subscribe/10 SSR订阅地址

## 可识别的SS(R)镜像

*/ssr-with-net-speeder

*/ss-with-net-speeder

*/shadowsocksr-docker
